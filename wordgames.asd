(defsystem "wordgames"
  :version "0" ; not released
  :description "Silly word games"
  :long-description "Helper to play at words with friends"
  :author "Francois-Rene Rideau"
  :license "MIT"
  :depends-on ("alexandria" "inferior-shell")
  :components ((:file "words")))
